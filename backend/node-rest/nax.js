require("dotenv").config();

const {
    Client,
    MirrorClient,
    MirrorConsensusTopicQuery,
    ConsensusTopicCreateTransaction,
    ConsensusMessageSubmitTransaction,
    ConsensusTopicInfoQuery
} = require("@hashgraph/sdk");

async function main() {
    const operatorPrivateKey = process.env.OPERATOR_KEY;
    const operatorAccount = process.env.OPERATOR_ID;
    const mirrorNodeAddress = process.env.MIRROR_NODE_ADDRESS;
    const nodeAddress = process.env.NODE_ADDRESS;

    if (operatorPrivateKey == null ||
        operatorAccount == null ||
        mirrorNodeAddress == null ||
        nodeAddress == null) {
        throw new Error("environment variables OPERATOR_KEY, OPERATOR_ID, MIRROR_NODE_ADDRESS, NODE_ADDRESS must be present");
    }

    const consensusClient = new MirrorClient(mirrorNodeAddress);
    console.log(consensusClient)
    // console.log(consensusClient)

    const client = Client.forTestnet();
    client.setOperator(operatorAccount, operatorPrivateKey);
    // console.log(client)

    const transactionId = await new ConsensusTopicCreateTransaction()
        .setTopicMemo("1011778 BC Unlimited")
        .setMaxTransactionFee(100000000000)
        .execute(client);

    console.log("transactionId", transactionId)

    const transactionReceipt = await transactionId.getReceipt(client);
    const topicId = transactionReceipt.getConsensusTopicId();

    console.log("transactionReceipt", transactionReceipt)

    console.log("topicId", topicId)

    mirror = new MirrorConsensusTopicQuery()
        .setTopicId(topicId)
        .subscribe(
            consensusClient,
            (message) => console.log(message.toString()),
            (error) => console.log(`Error: ${error}`)
        );
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC ULC 2017 TERM LOAN B`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 B C UNLIMITED LIABILI 02/16/2024`)
        .execute(client)
        
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC ULC / NEW RED FINANCE INC CALLABLE`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC UNLIMITED LIABILITY CO 144A 5% 10/15/2025`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC ULC / NEW RED FINANCE INC CALLABLE`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 B C UNLIMITED LIABILI 02/16/2024`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC UNLIMITED LIABILITY CO 144A 5% 10/15/2025`)
        .execute(client)
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId("0.0.193821")
        .setMessage(`1011778 BC UNLIMITED LIABILITY CO 144A 4.38% 01/15/2028`)
        .execute(client)
    
    const topicInfo = await new ConsensusTopicInfoQuery()
        .setTopicId(topicId)
        .execute(client);
    
}   


main();