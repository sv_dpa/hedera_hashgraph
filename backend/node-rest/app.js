const express = require("express")

var cors = require('cors')

const app = express();

const consensusRoutes = require("./api/routes/consensus")

app.use('/consensus', consensusRoutes);
app.use(cors());

module.exports = app;