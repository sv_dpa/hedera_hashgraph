const http = require("http");
const app  = require("./app");

const host = process.env.PORT || "192.168.2.211";
const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);
