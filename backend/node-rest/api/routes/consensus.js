require("dotenv").config();

const {
    Client,
    MirrorClient,
    MirrorConsensusTopicQuery,
    ConsensusTopicCreateTransaction,
    ConsensusMessageSubmitTransaction,
    ConsensusTopicInfoQuery
} = require("@hashgraph/sdk");

async function submitMessage(topicId, message) {
    const operatorPrivateKey = process.env.OPERATOR_KEY;
    const operatorAccount = process.env.OPERATOR_ID;
    const mirrorNodeAddress = process.env.MIRROR_NODE_ADDRESS;
    const nodeAddress = process.env.NODE_ADDRESS;

    if (operatorPrivateKey == null ||
        operatorAccount == null ||
        mirrorNodeAddress == null ||
        nodeAddress == null) {
        throw new Error("environment variables OPERATOR_KEY, OPERATOR_ID, MIRROR_NODE_ADDRESS, NODE_ADDRESS must be present");
    }
    const consensusClient = new MirrorClient(mirrorNodeAddress);
    //console.log(consensusClient)

    const client = Client.forTestnet();
    client.setOperator(operatorAccount, operatorPrivateKey);
    // console.log(client)

    const transactionId = await new ConsensusTopicCreateTransaction()
        .setTopicMemo("1011778 BC Unlimited")
        .setMaxTransactionFee(100000000000)
        .execute(client);

    mirror = new MirrorConsensusTopicQuery()
        .setTopicId(`0.0.${topicId}`)
        .subscribe(
            consensusClient,
            (message) => console.log(message.toString()),
            (error) => console.log(`Error: ${error}`)
        );
    
    new ConsensusMessageSubmitTransaction()
        .setTopicId(topicId)
        .setMessage(message)
        .execute(client)
}

const express = require("express")

const router = express.Router();

router.post('/create-message/', (req, res, next)=>{
    console.log(req)
    const id = 194581;
    
    if (id != 0){
        submitMessage(id, message);
        res.header("Access-Control-Allow-Origin", "*");
        res.status(200).json({
            message: `received Id ${id}`
        })
    }
})

module.exports = router;