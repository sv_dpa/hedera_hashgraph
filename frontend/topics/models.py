from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class TimeStampModel(models.Model):
    created_date = models.DateField(
        blank=True,
        db_column="CreatedDate",
        verbose_name="Create Date",
        help_text="Date on which the record was inserted.",
        editable=False,
        null=True,
    )
    updated_date = models.DateField(
        blank=True,
        editable=False,
        verbose_name="Update Date",
        help_text="Date on which the record was updated.",
        db_column="UpdatedDate",
        null=True,
    )

    class Meta:
        abstract = True


class ObjectStateModel(TimeStampModel):
    is_active = models.BooleanField(
        default=True,
        verbose_name="Is Active",
        help_text="This column is used for soft delete. Users can reactivate the entry via portal",
        db_column="IsActive",
        null=True,
    )
    is_delete = models.BooleanField(
        default=False,
        verbose_name="Is Delete",
        help_text="This column is used for hard delete. Users cannot reactivate the entry via portal",
        db_column="IsDeleted",
        null=True,
    )

    class Meta:
        abstract = True


# Create your models here.
class Topics(models.Model):
    topic_user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    topic_id = models.IntegerField(primary_key=True, blank=True,)
    shard_id = models.IntegerField(default=0, blank=True,)
    realm_id = models.IntegerField(default=0, blank=True,)
    topic_name = models.CharField(max_length=256, blank=True,)
    topic_memo = models.CharField(max_length=2560, blank=True,)

    def save(self, *args, **kwargs):
        """ On save, update timestamps """
        if not self.topic_id:
            self.create_date = timezone.now()
        self.update_date = timezone.now()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.realm_id}.{self.shard_id}.{self.topic_id} [{self.topic_name}]"