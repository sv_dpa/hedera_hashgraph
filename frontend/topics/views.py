from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponse

from .forms import CreateTopicForm, CreateMessageForm
from .models import Topics

class CreateTopics(View):
    template_name = "topics/create_topic.html"
    
    def get(self, request, *args, **kwargs):
        form = CreateTopicForm()
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        form = CreateTopicForm(request.POST)
        
        return render(request, self.template_name, locals())

class CreateMessage(View):
    template_name = "topics/create_message.html"
    
    def get(self, request, *args, **kwargs):
        topic = Topics.objects.all().values_list("topic_id", flat=True)
        print(topic)
        form = CreateMessageForm(topic=topic)
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        form = CreateMessageForm(request.POST)
        
        return render(request, self.template_name, locals())

def get_memo(request, pk):
    if request.is_ajax:
        try:
            memo = Topics.objects.get(topic_id=pk).topic_memo
            return HttpResponse(memo)            
        except: 
            return HttpResponse(404)
    return redirect("/")