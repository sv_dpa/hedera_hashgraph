from django import forms

from .models import Topics


class CreateTopicForm(forms.ModelForm):
    # consensusClient = forms.
    class Meta:
        model=Topics
        fields=[
            "topic_id",
            "topic_name",
            "topic_memo",
            
        ]
    
    def save(self):
        print(self.cleaned_data)


class CreateMessageForm(forms.ModelForm):
    
    topic_id = forms.ModelChoiceField(queryset=None)
    
    def __init__(self, *args, **kwargs):
        topic = kwargs.pop("topic")        
        super().__init__(*args, **kwargs)
        self.fields["topic_id"].queryset = topic
    
    class Meta:
        model=Topics
        fields=[
    
            "shard_id",
            "realm_id",
            "topic_name",
            "topic_memo",
            
        ]
    
    def save(self):
        print(self.cleaned_data)
