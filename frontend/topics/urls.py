from django.contrib import admin
from django.urls import path

from . import views


urlpatterns = [
    path("create-topic/", views.CreateTopics.as_view(), name="create_topics"),
    path("create-message/", views.CreateMessage.as_view(), name="create_message"),

    path("get-memo/<int:pk>", views.get_memo, name="get_memo"),
    
]