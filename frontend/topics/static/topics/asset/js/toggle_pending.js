//Check all pending items in tables and filter by Pending
$(document).ready(function () {
    var toggleClick = 1;
    $("#togglePending").click(function () {
        if (toggleClick) {
            //Display pending items
            $("#mainDataId tr").each(function (index, val) {
                console.log($(this).find("td").text());
                if ($(this).find("td").text().includes("Pending")) {
                    //Do nothing              
                }
                else {
                    $(this).hide();
                }

            });
            toggleClick = 0;
        }
        else {
            //Showing all Items
            $("#mainDataId tr").each(function (index, val) {
                $(this).show();
            });
            toggleClick = 1;
        }
    });
});