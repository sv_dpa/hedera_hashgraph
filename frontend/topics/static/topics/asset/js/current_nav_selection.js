$(document).ready(function() {
    //Checks the current url and changes class of nav to active accordingly
    currUrl = $(location).attr('href');
    if (currUrl.includes("create")){
      $('#timesheetList').addClass("active");
    }
    else if (currUrl.includes("report")){
      $('#reportList').addClass("active");
    }
    else if (currUrl.includes("manpower")){
      $('#manpowerList').addClass("active");
    }
    else if (currUrl.includes("approve-hours")){
      $('#teamAdminList').addClass("active");
    }
  });