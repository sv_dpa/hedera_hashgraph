from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from django.views import View
# from .forms import ProfileUpdateForm


# @login_required
# def profile(request):
#     """ 
#     Summary. 
  
#     This is function based view for rendering user profile page. 
#     Users can edit few details using this page. 
  
#     Parameters: 
#     request: Takes request from frontend. If request.method is POST and form is valid then saves the data to database.
  
#     Returns: 
#     page: renders an HTML page of with ProfileUpdateForm. 
  
#     """
#     if request.method == "POST":  # Check if the method to is POST
#         p_form = ProfileUpdateForm(
#             request.POST, request.FILES, instance=request.user.profile
#         )  # Send required information to Profile Update Form

#         if p_form.is_valid():
#             p_form.save()  # Save in the database
#             messages.success(request, f"Your account has been updated!")
#             return redirect("profile")
#     else:
#         p_form = ProfileUpdateForm(
#             instance=request.user.profile
#         )  # If it is not POST, then push the default or already uploaded pic for view

#     context = {"p_form": p_form}
#     return render(request, "user/profile.html", context)

